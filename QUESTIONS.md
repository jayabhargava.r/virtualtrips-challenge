# Questions

Q1: Explain the output of the following code and why

```js
setTimeout(function () {
  console.log("1");
}, 100);
console.log("2");
```

A1: The output will be

```
2
1
```

because there is a delay of 100 milli seconds in setTimeout function.

Q2: Explain the output of the following code and why

```js
function foo(d) {
  if (d < 10) {
    foo(d + 1);
  }
  console.log(d);
}
foo(0);
```

A2: the result will be

```
10
9
8
7
6
5
4
3
2
1
0
```

This is because, the foo method is called recursively until it satisfies the if condition and later the console will be printed in the stack execution order.

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
```

A3: The function foo will never be called because there is no function calling here. If we want to print the default response to be `5` we need to have `foo` at the end like this:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
foo();
```

Q4: Explain the output of the following code and why

```js
function foo(a) {
  return function (b) {
    return a + b;
  };
}
var bar = foo(1);
console.log(bar(2));
```

A4: The result will be `3` because funtion `foo` will return a function which will be assigned to `bar` which would look something like this:

```js
  function(b) {
    return 1 + b;
  }
```

Later when we call `bar(2)`, the variable `b` will be replaced with 2 which returns the result `3`

Q5: Explain how the following function would be used

```js
function double(a, done) {
  setTimeout(function () {
    done(a * 2);
  }, 100);
}
```

A5: This is a function with a callback as param. The `done` callback will be executed after 100 milliseconds, whenever the `double` method is called with a value and a function as params.
