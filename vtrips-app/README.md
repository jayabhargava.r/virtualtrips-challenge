# Virtualtrips Coding Challenge

- This project uses React on the front-end and express server to serve the data fetched from sqlite db.
- The database is stored under `server/db` folder.
- Connection establishment is in `server/db.js` file.
- Server routes are defined under `server/routes` folder.
- We make use of `server/controllers` folder to hold the business logic of specific API end points.
- Clientside logic is implemented under `src` folder.

# How does it work?

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
Enter the search term with 2 or more characters. Based on the search term the data will be listed below the search box.

## Available Scripts

In the project directory, you can run:

### `yarn`

Install all the dependency modules.

### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `server/build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
