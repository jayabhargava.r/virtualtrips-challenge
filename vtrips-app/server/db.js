const path = require('path')

const dbPath = path.resolve(__dirname, 'db/vtrips.sqlite3')

const knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: dbPath,
    },
    useNullAsDefault: true
})

module.exports = knex;