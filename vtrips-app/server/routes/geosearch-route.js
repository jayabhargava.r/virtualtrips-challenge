const express = require('express');
const geoSearchController = require('../controllers/geosearch-controller');

const router = express.Router();

router.get('/', geoSearchController);

module.exports = router;