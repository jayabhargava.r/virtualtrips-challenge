const express = require('express');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');

const geosearchRouter = require('./routes/geosearch-route');

const PORT = process.env.PORT || 4001;

const app = express();

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/locations', geosearchRouter);

app.use((err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Something is broken. - 500 status error')
});

app.use((req, res, next) => {
    res.status(404).send('Sorry we could not find that. - 404 status error')
});

app.listen(PORT, function() {
    console.log(`Server is running on: ${PORT}`)
});