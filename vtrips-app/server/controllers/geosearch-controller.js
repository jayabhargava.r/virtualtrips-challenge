const knex = require('./../db')

const geoSearch = async (req, res) => {
    const {term} = req.query;

    knex('geoname').where('name', 'like', `${term}%`)
    .select(['name', 'geonameid'])
    .orderBy('name', 'asc')
    .then(data => {
      res.json(data)
    })
    .catch(err => {
      res.json({ message: `There was an error searching locations: ${err}` })
    })
}

module.exports = geoSearch;