import { useState, useEffect } from 'react';

const useSearchResults = (searchTerm) => {
    const [locations, setLocations] = useState([]);
    const [error, setError] = useState(null);
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        const fetchLocations = async () => {
            setLoading(true);
            try {
                if(searchTerm.length >= 2)  {
                    // TODO: url to be moved to config
                    const res = await fetch(`http://localhost:4001/locations?term=${searchTerm}`);
                    const json = await res.json();
                    setLocations(json);
                } else {
                    setLocations([]);
                } 
            } catch(err) {
                setError(err);
                console.log('something went wrong', err);
            }
            setLoading(false);
        };
        fetchLocations()
    }, [searchTerm]);

    return { locations, error, isLoading };
}

export default useSearchResults;