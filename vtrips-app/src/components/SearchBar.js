import React, { useState } from 'react';

import ResultPanel from './ResultPanel';

const SearchBar = () => {
    const [searchTerm, setSearchTerm] = useState('');

    return (
        <div>
            <input data-testid="searchbar"
                type="text"
                placeholder="Search here"
                onChange={(e) => setSearchTerm(e.target.value)}
                value={searchTerm}
            />
            <br/>
            <br/>
            <ResultPanel searchTerm={searchTerm} />
        </div>
    )
}

export default SearchBar;