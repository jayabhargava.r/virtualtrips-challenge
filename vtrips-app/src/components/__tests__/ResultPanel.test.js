import React from 'react';
import ReactDOM from 'react-dom';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';

import ResultPanel from '../ResultPanel';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ResultPanel />, div);
});
