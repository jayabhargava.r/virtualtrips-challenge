import { act, renderHook } from "@testing-library/react-hooks";
import useSearchResults from "../UseSearchResults";

const getControlledPromise = () => {
  let deferred;
  const promise = new Promise((resolve, reject) => {
    deferred = { resolve, reject };
  });
  return { deferred, promise };
};

describe("useSearchResults", () => {
  it("fetches locations by the url constructed from searchterm", async () => {
    global.fetch = jest.fn();

    await act(async () => renderHook(() => useSearchResults("lon")));

    expect(global.fetch).toBeCalledWith(
      "http://localhost:4001/locations?term=lon"
    );
  });

  describe("while fetching data", () => {
    it("handles loading state correctly", async () => {
      const { deferred, promise } = getControlledPromise();

      global.fetch = jest.fn(() => promise);

      const { result, waitForNextUpdate } = renderHook(() => useSearchResults("lon"));

      expect(result.current.isLoading).toBe(true);
      deferred.resolve();

      await waitForNextUpdate();
      expect(result.current.isLoading).toBe(false);
    });
  });

  describe("when got data successfully", () => {
    it("handles successful state correctly", async () => {
      const { deferred, promise } = getControlledPromise();
      global.fetch = jest.fn(() => promise)

      const { result, waitForNextUpdate } = renderHook(() => useSearchResults("lon"));

      deferred.resolve({json: () => ({name: "germany"})})

      await waitForNextUpdate()

      expect(result.current.locations).toStrictEqual({name: "germany"})
    });
  });

  describe("with an error during request", () => {
    it("handles error state correctly", async () => {
      global.fetch = jest.fn(() => {
        return new Promise(() => {
          throw "Fetch error"
        })
      }) 

      const { result, waitForNextUpdate } = renderHook(() => useSearchResults("lon"));
      await waitForNextUpdate()

      expect(result.current.error).toStrictEqual("Fetch error")
    });
  });
});