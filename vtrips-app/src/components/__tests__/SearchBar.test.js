import React from 'react';
import ReactDOM from 'react-dom';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';

import SearchBar from '../SearchBar';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<SearchBar />, div);
});

it('renders searchbar correctly with empty value', () => {
    const { getByTestId } = render(<SearchBar />);
    expect(getByTestId('searchbar')).toHaveValue('');
});

it('renders searchbar correctly with some value', () => {
    const { getByTestId } = render(<SearchBar />);
    const inputElement = getByTestId('searchbar');
    const newValue = 'lon';
    fireEvent.change(inputElement, { target: { value: newValue} });
    expect(getByTestId('searchbar')).toHaveValue(newValue);
});

it('renders searchbar correctly with some value', () => {
    const { getByTestId } = render(<SearchBar />);
    const inputElement = getByTestId('searchbar');
    const newValue = 'lon';
    fireEvent.change(inputElement, { target: { value: newValue} });
    expect(getByTestId('searchbar')).toHaveValue(newValue);
});