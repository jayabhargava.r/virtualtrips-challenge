import React from 'react';
import useSearchResults from './UseSearchResults';

const ResultPanel = ({searchTerm}) => {
    const {locations, error, isLoading} = useSearchResults(searchTerm);

    if(isLoading) {
        return <span> Loading....</span>
    }

    if(error) {
        return <span> Network Error</span>
    }
    return (
        <div>
            <ul>
            {   locations.map((location, i) => {
                    return (
                        <li key={i}>
                            {location.name}
                        </li>
                    )
                })
            }
            </ul>
        </div>
    )
}

export default ResultPanel;