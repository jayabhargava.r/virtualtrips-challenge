import React from 'react';
import { render } from 'react-dom';

import SearchBar from './components/SearchBar';

const rootElement = document.getElementById('root');

render(<SearchBar />, rootElement);